package com.miretador.api.models.repositories;

import com.miretador.api.models.entities.Book.Book;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface BookRepository extends CrudRepository<Book, Long> {
    List<Book> findByTitle(String title);
}